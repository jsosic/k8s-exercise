# k8s-exercise

A simple nodejs app run in k8s.

## Building docker image

```
docker-compose build
```

## Running app

```
docker-compose up -d
curl http://localhost:8081/health
curl http://localhost:8081/listUsers
```
