const express = require('express');
const app = express();
const fs = require("fs");

app.get('/health', function (req, res) {
   console.log( "Healtcheck performed" );
   res.end( "OK" );
})

app.get('/ping', function (req, res) {
   console.log( "Healtcheck performed" );
   res.end( "OK" );
})

app.get('/listUsers', function (req, res) {
   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
      console.log( "Users listed from json file" );
      res.end( data );
   });
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})

const redis = require('redis');
const REDIS_ENABLE = (process.env.REDIS_ENABLE === 'true');
const REDIS_HOST = process.env.REDIS_HOST || '127.0.0.1';
const REDIS_PORT = process.env.REDIS_PORT || '6379';
if ( REDIS_ENABLE ) {
  const client = redis.createClient({
      host: REDIS_HOST,
      port: REDIS_PORT
  });

  client.on('error', err => {
      console.log(err);
  });
}
